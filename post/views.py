from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core import serializers as django_serializers

from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import redirect, get_object_or_404

from django.urls import reverse_lazy
from django.views import View
from django.views.generic import DetailView, ListView

from django.contrib import messages
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormMixin

from .models import Post

from taggit.models import Tag

from .forms import *

from rest_framework import viewsets, serializers as rf_serializers


class PostCreateView(LoginRequiredMixin, CreateView, SuccessMessageMixin):
    form_class = PostForm
    template_name = 'post/post-add.html'

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        post = Post.objects.filter(author=self.request.user).order_by('-created')[0]
        messages.add_message(self.request, messages.INFO, 'Post was crated')
        return reverse_lazy('post:post_detail', kwargs={'pk': post.pk})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tags'] = Tag.objects.all().order_by('name')
        return context


class PostEditView(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, UpdateView):
    form_class = PostForm
    template_name = 'post/post-edit.html'
    success_url = reverse_lazy('post:post_list')
    success_message = "Changes saved"
    queryset = Post.objects.all()

    def get_object(self, **kwargs):
        pk_ = self.kwargs.get("pk")
        return get_object_or_404(Post, pk=pk_)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tags'] = Tag.objects.all().order_by('name')
        return context


class PostDelView(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, DeleteView):
    form_class = PostForm
    success_url = reverse_lazy('post:post_list')
    template_name = "post/post-del.html"

    def get_object(self, **kwargs):
        pk_ = self.kwargs.get("pk")
        return get_object_or_404(Post, pk=pk_)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tags'] = Tag.objects.all().order_by('name')
        return context

    def get_success_url(self, **kwargs):
        messages.add_message(self.request, messages.INFO, 'Post was deleted')
        return reverse_lazy('post:post_list')


class PostDetailDisplay(DetailView):
    template_name = 'post/post-detail.html'
    queryset = Post.objects.all()

    def get_object(self, **kwargs):
        pk_ = self.kwargs.get("pk")
        return get_object_or_404(Post, pk=pk_)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tags'] = Tag.objects.all().order_by('name')
        context['form'] = CommentForm
        return context


class AddCommentAjax(View):
    form = CommentForm
    template_name = 'post/post_detail.html'

    def post(self, *args, **kwargs):
        post = get_object_or_404(Post, pk=self.kwargs.get('pk'))
        if self.request.is_ajax and self.request.method == 'POST':
            form = self.form(self.request.POST)
            if form.is_valid():
                instance = form.save(commit=False)
                instance.post = post
                instance.save()
                serialized_instance = django_serializers.serialize('json', [instance, ])
                return JsonResponse({'instance': serialized_instance}, status=200)
            else:
                return JsonResponse({'instance': self.form.errors}, status=400)
        return JsonResponse({'instance': ''}, status=400)


class PostDetailView(FormMixin, DetailView):
    model = Post
    form_class = CommentForm()
    template_name = 'post/post-detail.html'

    def get(self, request, *args, **kwargs):
        view = PostDetailDisplay.as_view()
        return view(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        view = AddCommentAjax.as_view()
        return view(request, *args, **kwargs)


class PostListView(ListView):
    paginate_by = 5
    model = Post
    template_name = 'post/post-list.html'
    ordering = '-created'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['posts'] = Post.objects.all()
        context['tags'] = Tag.objects.all().order_by('name')
        return context


class SearchResultsView(ListView):
    model = Post
    template_name = 'post/search-post.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        object_list = Post.objects.filter(
            Q(title__icontains=query) | Q(author__username__icontains=query)
        )
        object_set = set(object_list)
        return object_set

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tags'] = Tag.objects.all().order_by('name')
        return context


def hide_comment(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    post = get_object_or_404(Post, pk=comment.post.pk)
    if request.user == post.author:
        comment.hide_comment()
        return redirect('post:post_detail', pk=comment.post.pk)


class MyPostsView(ListView):
    paginate_by = 5
    model = Post
    template_name = 'post/my_posts.html'
    context_object_name = 'posts'

    def get_queryset(self):
        user = get_object_or_404(User, id=self.request.user.id)
        return Post.objects.filter(author__id__exact=user.id).order_by('-created')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tags'] = Tag.objects.all().order_by('name')
        return context


class MostCommentedPostsView(ListView):
    model = Post
    template_name = 'post/most_commented_posts.html'
    context_object_name = 'posts'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tags'] = Tag.objects.all().order_by('name')
        return context


class PostsByTag(ListView):
    paginate_by = 5
    model = Post
    template_name = 'post/posts_by_tag.html'
    context_object_name = 'posts'

    def get_queryset(self):
        tag = get_object_or_404(Tag, slug=self.kwargs.get('tag_slug'))
        return Post.objects.filter(tags__in=[tag]).order_by('-created')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tags'] = Tag.objects.all().order_by('name')
        context['tag'] = get_object_or_404(Tag, slug=self.kwargs.get('tag_slug'))
        return context


# Serializacja
# JSON {"key": "value"} -> Python object

# Deserializacja
# Python object -> JSON {"key": "value"}

class UserSerializer(rf_serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['id', 'username']


class PostSerializer(rf_serializers.ModelSerializer):
    author = UserSerializer()

    class Meta:
        model = Post
        fields = ['id', 'title', 'body', 'author', 'image']


class CommentSerializer(rf_serializers.ModelSerializer):
    post = PostSerializer()

    class Meta:
        model = Comment
        fields = ('post', 'id', 'author', 'text')


# ViewSets define the view behavior.
class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.good_comment.all()
    serializer_class = CommentSerializer
