from django.conf.urls import url
from django.urls import path, include
from .views import *

from django.conf import settings
from django.conf.urls.static import static
from rest_framework import routers


# import domyślnych widoków na potrzeby logowania
from django.contrib.auth import views as auth_views


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
# router.register(r'chats', PostViewSet)
router.register(r'post/', PostViewSet, basename='Post')
router.register(r'comment/', CommentViewSet, basename='Comment')

app_name = 'post'
urlpatterns = [
                  url(r'api/', include(router.urls)),
                  path("add/", PostCreateView.as_view(), name="post_add"),
                  path("edit/<int:pk>/", PostEditView.as_view(), name="post_edit"),
                  path("del/<int:pk>/", PostDelView.as_view(), name="post_del"),
                  path("add_comment/<int:pk>", AddCommentAjax.as_view(), name="add_comment"),
                  path("detail/<int:pk>/", PostDetailView.as_view(), name="post_detail"),
                  path("", PostListView.as_view(), name="post_list"),
                  path("search/", SearchResultsView.as_view(), name="search_post"),
                  path('by_tag/<slug:tag_slug>/', PostsByTag.as_view(), name='posts_by_tag'),
                  path('comment/<int:pk>/hide/', hide_comment, name='hide_comment'),
                  path('my_posts/', MyPostsView.as_view(), name='my_posts'),
                  path('most_commented_posts/', MostCommentedPostsView.as_view(), name='most_commented_posts'),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
