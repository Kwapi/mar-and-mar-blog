import datetime

from django.contrib.auth.models import User
from django.test import TestCase

from ..models import Post, Comment


class ModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        author = User.objects.create(username='Franek', password='asd123')
        Post.objects.create(title='post1',
                            body='post body',
                            author=author)

        Comment.good_comment.create(post=Post.objects.get(pk=1),
                                    text='Test text',
                                    author='Rysiek')

    def test_post_created(self):
        post = Post.objects.get(id=1)
        created_time = post.created.second
        now = datetime.datetime.now().second
        self.assertAlmostEqual(created_time, now)

    def test_post_title(self):
        post = Post.objects.get(id=1)
        self.assertEqual(post.title, 'post1')

    def test_post_body(self):
        post = Post.objects.get(id=1)
        self.assertEqual(post.body, 'post body')

    def test_post_author(self):
        post = Post.objects.get(id=1)
        self.assertNotEqual('Józek', post.author.username)
        self.assertEqual('Franek', post.author.username)

    def test_post_tags(self):
        post = Post.objects.get(id=1)
        post.tags.add('tag1', 'tag2', 'tag3')
        self.assertIn('tag2', post.tags.names())
        self.assertNotIn('tag5', post.tags.names())

    def test_comment_author(self):
        post = Post.objects.get(id=1)
        comment = post.comments.get(pk=1)
        self.assertEquals(comment.author, 'Rysiek')

    def test_comment_text(self):
        post = Post.objects.get(id=1)
        comment = post.comments.get(pk=1)
        self.assertEquals(comment.text, 'Test text')

    def test_comment_is_not_hidden(self):
        post = Post.objects.get(id=1)
        comment = post.comments.get(pk=1)
        self.assertFalse(comment.hidden)

    def test_comment_date_added(self):
        post = Post.objects.get(id=1)
        comment = post.comments.get(pk=1)
        created_time = comment.date_added.second
        now = datetime.datetime.now().second
        self.assertAlmostEqual(created_time, now)

    def test_comment_is_hidden(self):
        post = Post.objects.get(id=1)
        comment = post.comments.get(pk=1)
        comment.hide_comment()
        self.assertTrue(comment.hidden)