from django.test import TestCase, Client
from django.urls import reverse

from ..views import *


class TestViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.post_list = reverse('post:post_list')
        self.post_detail = reverse('post:post_detail', kwargs={'pk': 1})
        self.comment_url = reverse('post:comment_add', kwargs={'pk': 1})
        self.author1 = User.objects.create(username='Franek', password='asd123')
        self.post1 = Post.objects.create(title='Post 1', body='Test body', author=self.author1)
        Comment.good_comment.create(post=self.post1, text='Test text', author='John')

    def test_post_list(self):
        response = self.client.get(self.post_list)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'post/post-list.html')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'pagination.html')

    def test_post_detail(self):
        response = self.client.get(self.post_detail)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'post/post-detail.html')
        self.assertTemplateUsed(response, 'base.html')

    def test_add_comment_to_post(self):
        response = self.client.post(self.comment_url)

        self.assertEquals(response.status_code, 302)
        self.assertEquals(self.post1.comments.first().text, 'Test text')
        self.assertEquals(self.post1.comments.first().author, 'John')
        self.assertEquals(self.post1.comments.first().post, self.post1)
