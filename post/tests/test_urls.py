from django.test import SimpleTestCase
from django.urls import reverse, resolve

from ..views import PostListView, PostDetailView, PostCreateView, PostEditView, PostDelView, CommentAdd, hide_comment,\
    MyPostsView, MostCommentedPostsView, PostsByTag


class TestUrls(SimpleTestCase):

    def test_post_list_resolves(self):
        url = reverse('post:post_list')
        self.assertEquals(resolve(url).func.view_class, PostListView)

    def test_post_detail_resolves(self):
        url = reverse('post:post_detail', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func.view_class, PostDetailView)

    def test_post_add_resolves(self):
        url = reverse('post:post_add')
        self.assertEquals(resolve(url).func.view_class, PostCreateView)

    def test_post_edit_resolves(self):
        url = reverse('post:post_edit', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func.view_class, PostEditView)

    def test_post_add_resolves(self):
        url = reverse('post:post_del', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func.view_class, PostDelView)

    def test_comment_add_resolves(self):
        url = reverse('post:comment_add', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func.view_class, CommentAdd)

    def test_comment_hide_resolves(self):
        url = reverse('post:hide_comment', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, hide_comment)

    def test_my_posts_resolves(self):
        url = reverse('post:my_posts')
        self.assertEquals(resolve(url).func.view_class, MyPostsView)

    def test_most_commented_posts_resolves(self):
        url = reverse('post:most_commented_posts')
        self.assertEquals(resolve(url).func.view_class, MostCommentedPostsView)

    def test_posts_by_tag_resolves(self):
        url = reverse('post:posts_by_tag', kwargs={'tag_slug': 'dogs'})
        self.assertEquals(resolve(url).func.view_class, PostsByTag)