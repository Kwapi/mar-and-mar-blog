from django import template
from django.db.models import Count

from ..models import Post

register = template.Library()


@register.simple_tag
def get_most_commented_posts(count=5):
    return Post.objects.all().annotate(comments_num=Count('comments')).order_by('-comments_num')[:count]
