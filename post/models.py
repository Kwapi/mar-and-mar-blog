from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from account.models import Profile

from taggit.managers import TaggableManager


class Post(models.Model):
    title = models.CharField(max_length=300, verbose_name="Title")
    body = models.TextField(default="", verbose_name="Details")
    created = models.DateTimeField(editable=False, default=timezone.now)
    modified = models.DateTimeField(auto_now=True, editable=False)
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Username")
    image = models.ImageField(upload_to='blog_images/', null=True, blank=True, verbose_name="Add picture")
    tags = TaggableManager()

    def __str__(self):
        return f"Title: {self.title} writen by {self.author}"


class GoodCommentManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(hidden=False)


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
    text = models.TextField()
    author = models.CharField(max_length=150, default='Anonymous user')
    date_added = models.DateTimeField(auto_now_add=True)
    hidden = models.BooleanField(default=False)
    objects = models.Manager()
    good_comment = GoodCommentManager()

    class Meta:
        ordering = '-date_added',

    def hide_comment(self):
        self.hidden = True
        self.save()
