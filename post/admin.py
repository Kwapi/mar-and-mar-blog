from django.contrib import admin
from .models import Post, Comment


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'created', 'modified')
    list_filter = ('title', 'author', 'modified')
    search_fields = ('title', 'author', 'created', 'modified')


admin.site.register(Comment)
