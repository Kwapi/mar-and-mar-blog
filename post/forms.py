from .models import *
from django import forms

from tinymce.widgets import TinyMCE


class PostForm(forms.ModelForm):
    body = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30, 'nowrap': True}))

    class Meta:
        model = Post
        fields = ['title', 'tags', 'body', 'image']


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('author', 'text')
